package agendaenlazada;


public class ListaEnlazadaSimple {
	/**
	 * Una lista enlazada simple es una estructura de datos
	 * dinámica, es decir, su tamaño varía durante la ejecución 
	 * del programa.
	 * Se compone de una colección de nodos. Tendremos siempre
	 * una referencia que apunta al primer nodo de la lista. El final
	 * de la lista se reconoce porque el campo enlace del ultimo nodo
	 * contiene un null.
	 * Cada nodo tendrá 2 atributos: un campo información
	 * donde guardamos la referencia a un objeto.
	 * Y un campo enlace que guarda la referencia de otro nodo.
	 */
	
	//Referencia que apunta al primer nodo de la lista
	private Nodo inicio;
	
	/**
	 * Constructor
	 */
	
	public ListaEnlazadaSimple() {
		inicio = null;
	}
	
	public void anadirPrincipio(Object obj) {
		//Instanciamos un nuevo nodo
		Nodo nuevo = new Nodo(obj, inicio);
		//Inicio apuntará a nuevo nodo
		inicio = nuevo;
	}
	
	public void verLista() {
		//Nos situamos al principio de la lista
		Nodo actual = inicio;
		while(actual != null) {
			System.out.println(actual.info);
			//Avanzamos al siguiente nodo
			actual = actual.enlace;
		}
	}
	
	public boolean listaVacia() {
		/**
		 * Retorna un valor si la lista está vacía o no
		 */
		return (inicio == null);
	}
	
	public void anadirFinal(Object obj) {
		//Instanciamos un nuevo nodo
		Nodo nuevo = new Nodo(obj, null);
		//Comprobamos si la lista está vacía
		if(listaVacia())
			inicio = nuevo;
		else {
			//Recorremos la lista y nos situamos en el último elemento
			Nodo actual = inicio;
			while(actual.enlace != null) {
				//Avanza al siguiente nodo
				actual = actual.enlace;
			}
			//Enlazamos el ultimo nodo con el nuevo
			actual.enlace = nuevo;
		}
	}
	
	/**
	 * Eliminar un elemento de la lista
	 */
	
	public boolean eliminar(Object obj) {
		/**
		 * Recorremos la lista buscando un elemento que eliminar.
		 * Si lo encontramos, enlazamos el nodo anterior con el siguiente.
		 */
		
		// Referencia el nodo anterior
		
		Nodo anterior = null;
		Nodo actual = inicio;
		boolean encontrado = false;
		while (actual != null && !encontrado) {
			if (obj.equals(actual.info)) {
				encontrado = true;
			} else {
				// Avanzamos al siguiente nodo
				anterior = actual;
				actual = actual.enlace;
			}
		}
		// Comprobamos si lo hemos encontrado
		
		if (encontrado) {
			// Comprobamos si es el primero
			if (anterior == null) {
				// o actual == inicio
				// hacemos que inicio apunte al siguiente nodo
				inicio = inicio.enlace;
			} else {
				// enlazamos el nodo anterior con el siguiente
				anterior.enlace = actual.enlace;
			}
		}
		return encontrado;
	}
	///////////////////////////////////////////////////////////////
	/**
	 * Metodo particular para  buscar un contacto en la agenda
	 */
	public  Nodo busquedaContacto(Contacto contacto)
	{
		for(Nodo actual = inicio; actual != null; actual = actual.enlace)
		{
			if(contacto.equals((Contacto)actual.info))
				return actual;
		}
		return null;
	}
}
