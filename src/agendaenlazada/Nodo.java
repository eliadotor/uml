package agendaenlazada;

/**
 * Esta clase reperesnta un nodo de una lista enlazada generica
 */

public class Nodo{
	//atributos
	Object info;
	Nodo enlace;
	
	/**
	 * constructor
	 * @param info
	 * @param enlace
	 */
	public Nodo(Object info, Nodo enlace){
		this.info = info;
		this.enlace = enlace;
	}
}
