package agendaenlazada;

import java.util.Scanner;
/**
 * Clase para crear una agenda de contactos mediante una lista enlazada simple
 * @author Elia
 *
 */
public class AgendaEnlazada {
	//miembros dato
	private ListaEnlazadaSimple lista;	
	/**
	 * Constructor de la clase que crea un objeto de tipo lista enlazada simple
	 */
	public AgendaEnlazada(){
		lista = new ListaEnlazadaSimple();
	}
	
	/**
	 * Metodo para introducir los datos de un contacto y retorna un objeto de
	 * tipo contacto
	 * @return
	 */
	public Contacto introDatos(){
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca el nombre del contacto: ");
		String nombre = entrada.nextLine();
		System.out.println("Introduzca el numero de telefono: ");
		String telefono = entrada.nextLine();
		System.out.println("Introduzca el email: ");
		String email = entrada.nextLine();
		return new Contacto(nombre,telefono,email);
	}

	/**
	 * Metodo que se encarga de comprobar si la agenda esta vacia
	 */
	public boolean agendaVacia(){
		return lista.listaVacia();
	}
	
	/**
	 * Metodo que muestra el menu de opciones y se encarga de validar
	 * la opcion elegida
	 * @return
	 */
	public int verMenu(){
		System.out.println("Menú de operaciones: ");
		System.out.println("**********************");
		System.out.println("1. Añadir contacto");
		System.out.println("2. Editar contacto");
		System.out.println("3. Eliminar contacto");
		System.out.println("4. Consultar contacto");
		System.out.println("5. Ver toda la agenda");
		System.out.println("6. Abandonar la aplicación");
		System.out.println("Elija la opción: ");
		Scanner entrada = new Scanner(System.in);
		boolean opcionValida;
		int opcion;
		do{
			opcion = entrada.nextInt();
			opcionValida = (opcion >=1 && opcion <= 6);
			if(!opcionValida)
				System.err.println("La opcion debe estar entre 1 y 6");
		
		}while(!opcionValida);
		return opcion;	
	}
	////////////////////////////////////////////////////////////
	/**
	 * Metodo que se encarga de hacer posible el arranque de la aplicacion
	 */
	public void arrancarAplicacion(){
		boolean salir = false;	
		do{
			int opcion = verMenu();
			salir = (opcion == 6);
			if(!salir){
				switch(opcion){
					case 1:
						nuevoContacto();
						break;
					case 2:
						editarContacto();
						break;
					case 3:
						eliminarContacto();
						break;
					case 4:
						consultarContacto();
						break;
					case 5:
						verAgendaCompleta();
						break;
				}
			}			
		}while(!salir);
		System.out.println("Programa finalizado");		
	}
	
	/**
	 * Metodo para crear un contacto nuevo, despues de comprobar si el contacto existe
	 * previamente, en caso de que no exista crea el contacto
	 */
	public void nuevoContacto(){
		Contacto contacto = introDatos();
		//Compobamos si existe
		Nodo actual = lista.busquedaContacto(contacto);
		boolean existe = (actual != null);		
		if(existe){
			System.err.println("Este contacto ya existe ");

		}else{
			lista.anadirPrincipio(contacto);
			System.out.println("Contacto: " + contacto.getNombre() + " añadido a la agenda");
		}	
	}
	
	/**
	 * Creamos el metodo para editar un contacto comprbando que la agenda
	 * no este vacia y el contacto exista previamente
	 */
	public void editarContacto(){
		if(!agendaVacia()){
			Contacto contacto = introDatos();
			Nodo actual = lista.busquedaContacto(contacto);
			Scanner entrada = new Scanner(System.in);
			boolean existe = (actual != null);
			if(existe){
				boolean opcionValida;
				boolean salir = false;	
				do{
					System.out.println("*********************");
					System.out.println("1-Editar nombre");
					System.out.println("2-Editar telefono");
					System.out.println("3-Editar e-mail");
					System.out.println("4-Salir");
					System.out.println("*********************");
					System.out.println("Elige una opcion");
					
					int opcion = entrada.nextInt();
					salir = (opcion == 4);
					opcionValida = (opcion >= 1 && opcion <= 4);
					//limpiamos el buffer
					entrada.nextLine();
					Contacto contacto1 = (Contacto)actual.info;
					
					if(!salir && opcionValida){
						switch(opcion){
							case 1:
								System.out.println("Introduce el nuevo nombre: ");
								String nombreNuevo = entrada.nextLine();
								contacto1.setNombre(nombreNuevo);
								System.out.println("El nuevo nombre es:" + contacto1.getNombre());
								break;
							case 2:
								System.out.println("Introduce el nuevo telefono: ");
								String telefonoNuevo = entrada.nextLine();
								contacto1.setTelefono(telefonoNuevo);
								System.out.println("El nuevo telefono es:" +contacto1.getTelefono());
								break;
							case 3:
								System.out.println("Introduce el nuevo email: ");
								String emailNuevo = entrada.nextLine();
								contacto1.setEmail(emailNuevo);
								System.out.println("El nuevo email es:" +contacto1.getEmail());
								break;
							case 4:
								salir = true;
							}
					}else{
						System.err.println("La opcion elegida debe estar entre 1 y 4");
					}
					
				}while(!salir && !opcionValida);
			}else{
				System.err.println("El contacto no existe");
			}
		}else{
			System.err.println("No hay contactos en la agenda");
		}
	}
	
	/**
	 * Metodo que elimina un contacto de la agenda despues de comprobar
	 * que la agenda no este vacia y que el contacto exista previamente
	 */
	public void eliminarContacto(){
		//comprobamos si la agenda esta vacia
		if(!agendaVacia()){
			Contacto contacto = introDatos();
			Nodo actual = lista.busquedaContacto(contacto);
			boolean existe = (actual != null);
			if(!existe){
				System.err.println("El contacto no existe");
			}else{
				//Eliminamos el contacto
				if(lista.eliminar(actual.info)){
					System.out.println("El contacto ha sido eliminado");
				}
			}			
		}else{
			System.err.println("La agenda esta vacia");
		}
	}
	
	/**
	 * Metodo que consulta un contacto tras comprobar que la agenda no este vacia
	 * y el contacto exista y muestra la informacion de un contacto
	 */
	public void consultarContacto(){
		if(!agendaVacia()){
			//introducimos los datos del contacto
			Contacto contacto = introDatos();
			Nodo actual = lista.busquedaContacto(contacto);
			boolean existe = (actual != null);
			if(!existe){
				System.err.println("El contacto no existe");
			}else{
				//Mostramos la info del contacto
				Contacto contacto1 = (Contacto)actual.info;
				System.out.println("Datos del contacto: " + contacto.toString());
			}
		}else{
			System.err.println("La agenda esta vacia");	
		}
	}
	
	/**
	 * Metodo que muestra todo el contenido de la agenda 
	 */	
	public void verAgendaCompleta(){
		if(!agendaVacia()){
			lista.verLista();
		}else{
			System.err.println("La agenda esta vacia");
		}
	}
}