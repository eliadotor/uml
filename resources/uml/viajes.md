E05-03 Ejercicio UML 3. 
=======================

Viajes
------

<!--Elia Dotor Puente-->

```mermaid
    classDiagram
    Vuelo "0..*" -- "1" Avion
    Vuelo "0..*" -- "0..*" Pasajero
    Vuelo .. Billete : viaja
    Pasajero .. Billete : con

    class Vuelo{
        -fecha : Date
        -numAsientos : Integer
    }

    class Avion {
        -modelo : String
        -capacidad : Integer
    }

    class Pasajero{
        -nombre : String
        -apellido : String
        -edad : Integer
    }

    class Billete{
        -idAsiento : Integer
    }

```