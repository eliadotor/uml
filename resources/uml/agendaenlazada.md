P05-10 Ejercicio UML 5
======================

Mi primer diagrama de clases
----------------------------

<!--Elia Dotor Puente-->


``` mermaid
      classDiagram
      ListaEnlazadaSimple <|-- AgendaEnlazada
      ListaEnlazadaSimple "1" *-- "1..*" Nodo
      AgendaEnlazada "1" o-- "0..*" Contacto : contiene

      class ListaEnlazadaSimple{
        -inicio : Nodo
        +ListaEnlazadaSimple()
        +anadirPrincipio() void
        +verLista() void
        +listaVacia() boolean
        +anadirFinal() void
        +eliminar() boolean
        +busquedaSecuencial() Nodo
      }

      class AgendaEnlazada{
        -lista : ListaEnlazadaSimple 
        +AgendaEnlazada()
        +introDatos() Contacto
        +agendaVacia() boolean
        +verMenu() Integer
        +nuevoContacto() void
        +editarContacto() void
        +eliminarContacto() void
        +consultarContacto() void
        +verAgendaCompleta() void
      }

      class Nodo{
        -info: Object
        -enlace: Nodo
        +Nodo()
      }

      class Contacto{
        -nombre : String
        -telefono : String
        -email : String
        +Contacto()
        +getNombre() String
        +setNombre() void
        +getTelefono() String
        +setTelefono() void
        +getEmail() String
        +setEmail() void
        +toString() String
        +equals() boolean
        +compareTo() Integer
   }

```