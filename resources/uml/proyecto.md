E05-04 Ejercicio UML 4
======================

Proyecto
--------

<!--Elia Dotor Puente-->


``` mermaid
      classDiagram
      Proyecto o-- "1..*" Ciclo : {orderer}
      Ciclo -- Ejecutable
      Ciclo o-- "4" Fase : {orderer}
      Fase o-- "1..*" Iteración : {orderer}
      Iteración o-- "1..*" Actividad
      Iteración -- "1..*" Artefacto
      Actividad "0..*" -- "0..*" Recurso
      Recurso <|-- Humano
      Recurso <|-- Material
      Artefacto <|-- Documento
      Artefacto <|-- Software

      class Proyecto{
        -nombre : String
        -avance : Float
      }

      class Ejecutable{
        -bytes
      }

      class Fase{
        -tipo : TipoFase
      }

      class Iteración{
        -inicio : Date
      }

      class Documento{
        -nombre : String
      }

      class Software{
        -bytes
      }

      class Actividad{
        -duración : Integer
        -avance : Float
      }

      class Humano{
        -nombre : String

      }

      class Material{
        -inventario : String
      }

      class TipoFase{
          inicio
          elaboración
          construccción
          transición
      }
```