P05-10 Ejercicio UML 5
======================

Mi primer diagrama de clases
----------------------------

<!--Elia Dotor Puente-->

#StarUML

##AgendaEnlazada

**Staruml** me parece una herramienta bastante rápida y fácil de usar, que permite la creación de diagramas de clases.
En un primer vistazo a la interfaz se ven las funciones principales del programa. Me ha parecido bastante intuitivo y que cuenta con todas las funciones (que hemos visto hasta ahora) de modelado de uml.

Durante la creación del diagrama,lo primero que hemos hecho ha sido crear un diagrama de clases, posteriormente hemos añadido las cuatro clases que necesitábamos *clase ListaEnlazadaSimple*, *clase AgendaEnlazada*, *clase Nodo* y *claseContacto*, añadiendo a cada una de ellas los atributos y los métodos que las componian.
Para ello la herramienta, al pinchar sobre la propia clase, te deja añadir tantos atributos, como métodos necesites y es bastante sencillo.

A continuación hemos establecido las relaciones que había entre cada una de las clases. Esto simplemente consiste en seleccionar el tipo de relación y unir ambas clases.

En nuestro caso, la clase *AgendaEnlazada* tiene una relaciçon de **herencia** con la clase *ListaEnlazadaSimple*. Al mismo tiempo, la *ListaEnlazadaSimple* está compuesta por varios *Nodos* por lo que tienen una relación de **composición**. En cuanto a la clase *Contacto*, la relación que tiene con la clase *AgendaEnlazada es de **agregación**, una agenda está formada por varios contactos, pero si la agenda desaparece, no lo hacen los contactos.

[StarUML_Agenda_Enlazada](img/agendaEnlazada.png)