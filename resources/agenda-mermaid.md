P05-10 Ejercicio UML 5
======================

Mi primer diagrama de clases
----------------------------

<!--Elia Dotor Puente-->

#Mermaid

##AgendaEnlazada

**Mermaid** me parece muy fácil de usar, puesto que solo es necesario aprender una sintáxis y unos conceptos básicos.
Permite visualizar tus proyectos en forma de diagrama y requiere muy poco tiempo para crearlos.
Si es cierto que hecho en falta poder tener mayor decisión sobre el resultado final del diagrama y no que esté tan limitado, pero no podemos olvidar que solo disponemos de la sintáxis para crearlos y que con esto no es posible alcanzar la precisión que te puede ofrecer una herrmanienta de diseño.

Durante la creación del diagrama,lo primero que hemos hecho ha sido indicar que ibamos a crear un diagrama con mermaid:  

```
(``` mermaid
      classDiagram
      
 ```)  
 ```

Posteriormente hemos añadido las cuatro clases que necesitábamos *clase ListaEnlazadaSimple*, *clase AgendaEnlazada*, *clase Nodo* y *claseContacto*, añadiendo a cada una de ellas los atributos y los métodos que las componian.
Para ello hemos utilizado la sintáxis que conocemos (-) para declarar privados los atributos y (+) para declarar públicos los atributos y cada uno de ellos seguidos del tipo de dato del que se trataba.

A continuación hemos establecido las relaciones que había entre cada una de las clases. 

En nuestro caso, la clase *AgendaEnlazada* tiene una relaciçon de **herencia** con la clase *ListaEnlazadaSimple* (--|>). Al mismo tiempo, la *ListaEnlazadaSimple* está compuesta por varios *Nodos* por lo que tienen una relación de **composición** (*--). En cuanto a la clase *Contacto*, la relación que tiene con la clase *AgendaEnlazada es de **agregación** (o--), una agenda está formada por varios contactos, pero si la agenda desaparece, no lo hacen los contactos.

Hemos establecido también la cardinalidad (1, 0..*, 1..*)

[Mermaid_Agenda_Enlazada](uml/agendaenlazada.md)