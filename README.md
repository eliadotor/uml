#Elia Dotor Puente - UML 

##E05-01 Ejercicio UML 1 

###Empresa-empleado-cliente

Creamos un diagrama de clases de una empresa, sus empleados y sus clientes.
Es importante tener en cuenta que tanto la *clase Empleado* como la *clase Cliente*
heredan de la *clase Persona*.  

A su vez, un *Empleado* puede ser directivo por lo que creamos la *clase Directivo*
o subordinado de otro empleado. 

La clase empleado tiene una *relación reflexiva*, es decir, consigo misma.

La relación entre *Empleado* y *Empresa* es de **composición** ya que no pueden existir empleados si no existe una empresa.  
 
Mientras que la relación entre *Cliente* y *Empresa* es de **agregación** porque en este caso el cliente no depende de la existencia de la empresa.


________________________________________________________________________________________

##E05-02 Ejercicio UML 2

###Biblioteca

Creamos un diagrama de clases de una biblioteca. En el tendremos la *clase Libro*, la *clase Autor*, 
la *clase Copia*, la *clase Lector*, la *clase multa* y la *clase préstamo*.

En este diagrama todas las relaciones son de **asociación**, excepto la de préstamo con *lector* y *copia* que es una relación de **dependencia**. No existirián préstamos si no hubiese copias de los libros y lectores.  

Tenemos que crear dos tipos **enumerados**, uno para el género del libro y otro para el estado del ejemplar, en los que vamos a especificar los diferentes valores que pueden tomar. Una vez que se han definido, se van a utilizar como el tipo de un atributo, igual que un tipo de dato.
________________________________________________________________________________________

##E05-03 Ejercicio UML 3

###Viajes

Creamos un diagrama de clases de una compañia aerea. En el tendremos la *clase Vuelo*, la *clase Aavión* y  
la *clase Pasajero*.

En este diagrama las relaciones tanto de *vuelo-avión*, como de *vuelo-pasajero* son de **asociación**, sin embargo, la relación que tiene *billete* tanto con el *pasajero* como con el *vuelo*, es una relación de **dependencia**. No existirián billetes si no hubiese pasajeros que viajan en vuelos.
________________________________________________________________________________________  

##E05-04 Ejercicio UML 4

###Proyecto

Creamos un diagrama de clases de un proyecto informático. En el tendremos la *clase Proyecto*, la *clase Ciclo*, la *clase Ejecutable*, la *clase Fase*, la *clase Iteración*, la *clase Actividad*, la *clase Artefacto*, la *clase Recurso*, la *clase Humano*, la *clase Material*, la *clase Artefacto* y 
la *clase Software.

En este diagrama las relaciones tanto de **Ciclo-Fase**, como de **Fase-Iteración** e **Iteración-Actividad** son de *agregación*, ya que los ciclos son conjuntos de Fases, las Fases de Iteraciones y las Iteraciones de Actividades, pero no dependen de la clase contenedora, sino que pueden seguir existiendo aunque estás desaparezcan; sin embargo, las relaciones de **Recurso-Humano**, **Recurso-Material** y **Artefacto-Documento**, **Artefacto-Software** son de *herencia*.

Creamos un tipo *enumerado*, para el tipo de fase, en el que vamos a especificar los diferentes valores que puede tomar. Una vez definido, lo utilizamos como el tipo del atributo del tipo de fase.
________________________________________________________________________________________

##P05-10 Mi primer diagrama de clases

###Agenda Enlazada

**Staruml** me parece una herramienta bastante rápida y fácil de usar, que permite la creación de diagramas de clases.
En un primer vistazo a la interfaz se ven las funciones principales del programa. Me ha parecido bastante intuitivo y que cuenta con todas las funciones (que hemos visto hasta ahora) de modelado de uml.

[Experiencia_StarUML](resources/agenda-staruml.md)


**Mermaid** me parece muy fácil de usar, puesto que solo es necesario aprender una sintáxis y unos conceptos básicos.
Permite visualizar tus proyectos en forma de diagrama y requiere muy poco tiempo para crearlos.
Si es cierto que hecho en falta poder tener mayor decisión sobre el resultado final del diagrama y no que esté tan limitado, pero no podemos olvidar que solo disponemos de la sintáxis para crearlos y que con esto no es posible alcanzar la precisión que te puede ofrecer una herrmanienta de diseño.

[Experiencia_Mermaid](resources/agenda-mermaid.md)

Personalmente, me ha gustado más la espericiencia con *StarUML* porque dispones de más herramientas a la hora de crear el diagrama de clases. 
En cuanto al diseño no tiene las limitaciones que tienen *mermaid* y puedes ser más minucioso con el resultado que quieres obtener.
Te permite adaptarte más a la realidad de tu proyecto y puedes hacer todas las especificaciones que consideres oportunas al poder incluir texto en cualquier lugar del diagrama.

Además, la herramienta te permite documentar el proyecto al mismo tiempo.

Si bien, si el tiempo fuese un factor importante a tener en cuenta, utilizaría *mermaid* ya que es mucho más rápido y no requiere más que un editor de texto y conocer la sintáxis.

Otro punto a favor es que *gitlab* lleva integrado una herramienta que permite pintar los diagramas creados con *mermaid*, lo cual me parece de mucha utilidad para tu proyecto.

Como siempre sucede, ambas herramientas tienen sus ventajas y desventajas, por lo que deberemos decidir cuál se ajusta más a nuestras necesidades dependiendo del proyecto y de las circustancias que tengamos en ese momento.

[Prácticas UML Elia Dotor Puente](https://gitlab.com/eliadotor/uml)